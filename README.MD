# Semantic Release Docker Image

## Introduction
This image is a containerization of semantic-release
It inspects commits messages and automatically creates/pushes tags in SemVer format back to the repo.


## Summary
 - Developer commits code with commit messages following Angular's conventional commits spec.
 - Tool inspects commit messages as code gets merged and does the following:
    - Generates a git tag following the Semver spec, while inferring from the commit message.
    - Pushes generated tag back to repo.
    - Generates Gitlab release notes

| Commit message                                                                                                                                                                                   | Release type               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |

## Included plugins (additional):
- @semantic-release/gitlab: Connector to gitlab
- @semantic-release/release-notes-generator: Generates release notes

## How to use
1. Ensure you have .releasesrc.json in the root of your project. \
   See here for more details on configuration [Semantic Release Configuration](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration) \
   The basic configuration below watches for merges to "master" branch
   
```json
{
  "branches": [ "master" ],
  "plugins": [
    ["@semantic-release/gitlab", {
      "gitlabUrl": "https://gitlab.com"
    }],
    ["@semantic-release/release-notes-generator", {}]
  ]
}
```
2. Configure your gitlab ci pipeline to use Semantic Release \
   Example .gitlab-ci.yml
   
```yaml
stages:
  - release

release:
  - image: registry.gitlab.com/iclchan/semantic-release-docker:latest
  - script:
      - semantic-release
```

3. Set environment variable GL_TOKEN to be the access token with push permissions to your repo.

   
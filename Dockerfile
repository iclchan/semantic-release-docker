FROM alpine:latest

RUN apk update && apk add bash git nodejs npm

RUN npm install -g semantic-release @semantic-release/gitlab @semantic-release/release-notes-generator

ENTRYPOINT ["/bin/bash", "-l", "-c"]